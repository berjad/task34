import React from 'react';
import LoginForm from '../forms/LoginForm'

const Login = () => {

  const handleLoginClicked = () => {
    console.log('Triggered from loginForm');
    
  }

    return (
        <div className="container">
          <h2>Login to Survey Puppy</h2>

          <LoginForm click={handleLoginClicked}/>
        </div>
      );
    };
    
    export default Login;