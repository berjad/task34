import React from "react";

const Dashboard = () => (
  <div className="container">
    <h2>Welcome to the Dashboard</h2>
    <p>Don't really know a good Dashboard message! <span role="img" aria-label="grin"> 😁 </span></p>
  </div>
);

export default Dashboard;
